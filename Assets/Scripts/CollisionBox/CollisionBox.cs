﻿using System;
using UnityEngine;

public class CollisionBox
{
    public int PlayerIndex { set; get; } = 0;
    public CollisionBoxInfo BoxInfo { protected set; get; }
    public void RecycleBox(CollisionBoxInfo boxInfo)
    {
        BoxInfo = boxInfo;
    }
}
