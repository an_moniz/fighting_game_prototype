﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBoxInfo
{
    private UInt32 tickStart;
    private UInt32 tickEnd;
    public Vector2 Position { private set; get; }
    public Vector2 Size { private set; get; }
}
