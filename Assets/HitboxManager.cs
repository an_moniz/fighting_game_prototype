﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxManager : MonoBehaviour
{
    private GameObject _poolRoot;

    private List<Hitbox> _hitboxes;
    void Awake()
    {
        _poolRoot = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateHitbox(CollisionBoxInfo boxInfo, HitboxInfo hitboxInfo)
    {

    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        for (int i = 0; i < _hitboxes.Count; ++i)
        {
            Gizmos.DrawWireCube(_hitboxes[i].BoxInfo.Position, _hitboxes[i].BoxInfo.Size);
        }
    }
}
